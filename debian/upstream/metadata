Repository: https://github.com/COMBINE-lab/salmon.git
Repository-Browse: https://github.com/COMBINE-lab/salmon
Bug-Database: https://github.com/COMBINE-lab/salmon/issues
Bug-Submit: https://github.com/COMBINE-lab/salmon/issues/new
Reference:
 - Author: Rob Patro and Geet Duggal and Michael I Love and Rafael A Irizarry and Carl Kingsford
   Title: Salmon provides fast and bias-aware quantification of transcript expression
   Journal: Nature Methods
   Year: 2017
   Month: March
   Volume: 14
   Number: 4
   Pages: 417-419
   DOI: 10.1038/nmeth.4197
   URL: https://www.nature.com/articles/nmeth.4197
   eprint: http://europepmc.org/backend/ptpmcrender.fcgi?accid=PMC5600148&blobtype=pdf
 - Author: Avi Srivastava and Laraib Malik and Tom Smith and Ian Sudbery and Rob Patro
   Title: Alevin efficiently estimates accurate gene abundances from dscRNA-seq data
   Journal: Genome Biology
   Year: 2019
   Month: March
   Volume: 20
   Number: 1
   DOI: 10.1186/s13059-019-1670-y
   URL: https://genomebiology.biomedcentral.com/articles/10.1186/s13059-019-1670-y
   eprint: https://genomebiology.biomedcentral.com/track/pdf/10.1186/s13059-019-1670-y
Registry:
 - Name: OMICtools
   Entry: OMICS_09075
 - Name: bio.tools
   Entry: salmon
 - Name: SciCrunch
   Entry: SCR_017036
 - Name: conda:bioconda
   Entry: salmon
 - Name: guix
   Entry: salmon
